import { shallowMount } from '@vue/test-utils'
import Search from '@/components/Search.vue'

test("emite evento click", async () => {
  const wrapper = shallowMount(Search);

  expect(wrapper.emitted().search).toBe(undefined)

  // Simular lo que hace el usuario.
  const input = wrapper.find('#textboxSearch')
  const submit = wrapper.find('button')
  input.setValue("alice")
  console.log(wrapper.vm.searchItem)
  submit.trigger('click')
  await wrapper.vm.$nextTick()

  // Comprobar la emisión.
  expect(wrapper.emitted().search.length).toBe(1)
  expect(wrapper.emitted().search[0]).toEqual(["alice"]) 

  // Comprobar que vuelve a vaciarse el textbox
  expect(wrapper.vm.searchItem).toBe(null)
});
