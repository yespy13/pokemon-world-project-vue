import { shallowMount } from "@vue/test-utils";
import RegionList from "@/components/RegionList.vue";
//primero test de pintar en pantalla
test("draw list Of Regions", () => {
  const listRegions = [{ name: 'region1' },{name:'region2'}];
  const wrapper = shallowMount(RegionList, {
    propsData: {
      listRegions: listRegions
    }
  });
  // expect(wrapper.find("option").text()).toBe("Hola");
  //expect(wrapper.findAll("option").length).toBe(listRegions.length);

  const options= wrapper.findAll('option').wrappers

  //expect(options.length).toBe(2)
  expect(options[0].text()).toBe('region1')
  expect(options[1].text()).toBe('region2')

});

//Aqui test de evento click
test("emite evento click", async () => {
  
  const wrapper = shallowMount(RegionList)

expect(wrapper.emitted().regionclick).toBe(undefined)

  // Simular lo que hace el usuario.
  
  const option = wrapper.find('option')
 option.trigger('click')
  await wrapper.vm.$nextTick()

  // Comprobar la emisión.
  expect(wrapper.emitted().regionclick.length).toBe(1)
  

  // Comprobar que vuelve a vaciarse el textbox
  expect(wrapper.vm.searchItem).toBe(null)
});