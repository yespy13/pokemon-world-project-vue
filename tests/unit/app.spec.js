import { shallowMount } from '@vue/test-utils'
import App from '@/App.vue'
import Buscador from '@/components/Search.vue'

test.skip("Comprueba que cambia de Pokémon cuando emite el hijo.", async () => {
  const wrapper = shallowMount(App, {
    data () {
      return {
        searchItem: 'Flareon'
      }
    }
  });

  const search = wrapper.find(Buscador)

  expect(wrapper.vm.searchItem).toBe('Flareon')

  search.vm.$emit('search', 'Pikachu')

  expect(wrapper.vm.searchItem).toBe('Pikachu'.toLowerCase())
  expect(wrapper.vm.url).toBe('https://pokeapi.co/api/v2/pokemon/Pikachu'.toLowerCase())
})

test("Comprueba que cambia de Región cuando emite el hijo.", async () => {
  const wrapper = shallowMount(App, {
    data () {
      return {
        searchItem: 'Kanto'
      }
    }
  });

  const search = wrapper.find(Buscador)

  expect(wrapper.vm.searchItem).toBe('Kanto')

  search.vm.$emit('search', 'Johto')

  expect(wrapper.vm.searchItem).toBe('Johto'.toLowerCase())
  expect(wrapper.vm.url).toBe('https://pokeapi.co/api/v2/pokemon/Johto'.toLowerCase())
})